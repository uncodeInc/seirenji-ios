//
//  AppDelegate.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/24.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import AVFoundation

var mPlayerRin: AVAudioPlayer!
var isGetPoint = false


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // ステータスバーの色 iPhone6ではinfo.plistだけでは効かない。
        UIApplication.shared.statusBarStyle = .lightContent
        // Override point for customization after application launch.
        // ナビゲーションバーの色
        UINavigationBar.appearance().barTintColor   = UIColor(red: 148/255, green: 104/255, blue: 169/255, alpha: 1.0)
        // ナビゲーションバーボタンのベースの色（設定アイコンの色など）
        UINavigationBar.appearance().tintColor      = UIColor.white
        // ナビゲーションバーのタイトル文字色
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
//        // ナビゲーションバーの背景色
//        UINavigationBar.appearance().backgroundColor = UIColor.clear
//        // ナビゲーションバーの戻るボタン非表示
////        let backButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
//
//        let attrs = [
//            NSAttributedStringKey.foregroundColor: UIColor.white,
//            NSAttributedStringKey.font: UIFont(name: "Hiragino Mincho ProN W6", size: 16)!
//        ]
//        UINavigationBar.appearance().titleTextAttributes = attrs
        
        //鈴の音をバッファに読み込んでおく
        let urlRin = Bundle.main.url(forResource: "rin", withExtension: "m4a")!
        do {
            try mPlayerRin = AVAudioPlayer(contentsOf: urlRin)
            mPlayerRin.stop()
            mPlayerRin.prepareToPlay()
        } catch {
            print(error)
        }
        return true
        
        //    --------------------------------------------------------------------------------
        //    ImageとPhoto選択された方を最初に表示する処理
        //    --------------------------------------------------------------------------------
//        var flg = true //分岐条件
//
//        var storyboard:UIStoryboard =  UIStoryboard(name: "Choice",bundle:nil)
//        var viewController:UIViewController
//
//
//        //表示するビューコントローラーを指定
//        if  flg {
//            viewController = storyboard.instantiateViewController(withIdentifier: "PhotoView") as UIViewController
//        } else {
//            viewController = storyboard.instantiateViewController(withIdentifier: "ImageView") as UIViewController
//        }
//
//
//        window?.rootViewController = viewController
//
//        return true
        //    --------------------------------------------------------------------------------
        //    ImageChoiceViewControllerを初回だけ表示にする処理
        //    --------------------------------------------------------------------------------
        
        // 1.userDefaultで、フラグを保持
        //    let userDefault = UserDefaults.standard
        
        // 2."firstlaunch"をキーに、Bool値を保持
        //    let dict = ["firstLaunch": true]
        
        // 3.デフォルト値を登録
        
        
        // 4."firstLaunch"に紐づく値がtrueなら(=初回起動)、値をfalseに更新して処理を行う
        
        
    }
    
    //  日付けが変わったらfalseに切り替わる。。。はず
    func applicationSignificantTimeChange(_ application: UIApplication) {
                isGetPoint = false
                print("呼ばれた？")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        mPlayerRin.stop()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

