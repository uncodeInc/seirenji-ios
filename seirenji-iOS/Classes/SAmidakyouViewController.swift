//
//  SAmidakyouViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/26.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import AVFoundation
import KRProgressHUD

extension UIImage {
    func cropping(to: CGRect) -> UIImage? {
        var opaque = false
        if let cgImage = cgImage {
            switch cgImage.alphaInfo {
            case .noneSkipLast, .noneSkipFirst:
                opaque = true
            default:
                break
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(to.size, opaque, scale)
        draw(at: CGPoint(x: -to.origin.x, y: -to.origin.y))
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}

class SAmidakyouViewController: UIViewController, AVAudioPlayerDelegate {
    enum KYOUTEN {
        case shoushinge
        case amidakyou
        case sanbutuge
        case jyuseige
        case shounin
    }
    
    var kyouten: KYOUTEN? = KYOUTEN.sanbutuge
    
    @IBOutlet weak var oOkyouView: UIImageView!
    @IBOutlet weak var oPlayButton: UIButton!
    @IBOutlet weak var oSlider: UISlider!
    
    var mOkyouSound: AVAudioPlayer!
    var mOkyouImg: UIImage!
    var mPageWidth: Float! = 292.0
    var mTitleView: UIView!
    
    var mTimer: Timer? = nil
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBarButton作成
        self.navigationItem.hidesBackButton = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = "讃仏偈"
        
        self.mTitleView  = UIView.init(frame: CGRect(x: 0, y: 0, width: self.oOkyouView.frame.width, height: self.oOkyouView.frame.height))
        self.mTitleView.backgroundColor = UIColor(red: 231 / 255, green: 174 / 255, blue: 87 / 255, alpha: 1)
        self.mTitleView.layer.borderColor = UIColor.white.cgColor
        self.mTitleView.layer.borderWidth = 3.0
        let titleHeight = self.oOkyouView.frame.height * 0.8
        let titleWidth = titleHeight / 7.0 * 2.0
        let title: UIImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: titleWidth, height: titleHeight))
        
        var urlOkyo = Bundle.main.url(forResource: "sanbutuge", withExtension: "m4a")!
        self.oOkyouView.image = UIImage(named: "sanbutuge.png")
        title.image = UIImage(named: "menu_sanbutuge.png")
        
        switch self.kyouten! {
        case .shoushinge:
            self.navigationItem.title = "正信偈"
            urlOkyo = Bundle.main.url(forResource: "shoushinge", withExtension: "m4a")!
            self.oOkyouView.image = UIImage(named: "shoushinge.png")
            title.image = UIImage(named: "menu_shoushinge.png")
            break
        case .amidakyou:
            self.navigationItem.title = "仏説阿弥陀経"
            urlOkyo = Bundle.main.url(forResource: "amidakyo", withExtension: "m4a")!
            self.oOkyouView.image = UIImage(named: "amidakyou.png")
            title.image = UIImage(named: "menu_amidakyou.png")
            break
        case .jyuseige:
            self.navigationItem.title = "重誓偈"
            urlOkyo = Bundle.main.url(forResource: "jyuuseige", withExtension: "m4a")!
            self.oOkyouView.image = UIImage(named: "jyuuseige.png")
            title.image = UIImage(named: "menu_jyuseige.png")
            break
        case .shounin:
            self.navigationItem.title = "聖人一流章"
            urlOkyo = Bundle.main.url(forResource: "shounin", withExtension: "m4a")!
            self.oOkyouView.image = UIImage(named: "shounin.png")
            title.image = UIImage(named: "menu_shounin.png")
            break
        default: break
        }
        self.mTitleView.addSubview(title)
        title.center = self.mTitleView.center
        self.oOkyouView.addSubview(self.mTitleView)
        
        
        do {
            try self.mOkyouSound = AVAudioPlayer(contentsOf: urlOkyo)
            
            //音楽をバッファに読み込んでおく
            self.mOkyouSound.delegate = self
            self.mOkyouSound.prepareToPlay()
            self.oSlider.maximumValue = Float(mOkyouSound.duration)
            self.oSlider.value = 0.0
            
        } catch {
            print(error)
        }
        
        self.mOkyouImg = self.oOkyouView.image
    }
    
    //    --------------------------------------------------------------------------------
    //    viewWillDisappear(_ animated:)
    //    --------------------------------------------------------------------------------
    override func viewWillDisappear(_ animated: Bool) {
        self.mOkyouSound.stop()
        self.mTitleView = nil
    }
    
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    playBtnAction(sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func playBtnAction(sender: UIButton) {
        self.mTitleView.removeFromSuperview()
        if self.mOkyouSound.isPlaying {
            self.mOkyouSound.pause()
            self.oPlayButton.setImage(UIImage(named: "btn_play.png")!, for: UIControlState.normal)
            if self.mTimer != nil {
                self.mTimer?.invalidate()
                self.mTimer = nil
            }
        } else {
            self.mOkyouSound.currentTime = TimeInterval(self.oSlider.value)
            self.mOkyouSound.play()
            self.oPlayButton.setImage(UIImage(named: "btn_stop.png")!, for: UIControlState.normal)
            if self.mTimer == nil {
                self.mTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
            }
        }
    }
    
    //    --------------------------------------------------------------------------------
    //    sliderAction(sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func sliderAction(sender: UISlider) {
        self.mOkyouSound.pause()
        self.oPlayButton.setImage(UIImage(named: "btn_play.png")!, for: UIControlState.normal)
        self.updateTime()
    }
    
    //  MARK: - Delegate
    //    --------------------------------------------------------------------------------
    //    audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    //    --------------------------------------------------------------------------------
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.oPlayButton.setImage(UIImage(named: "btn_play.png")!, for: UIControlState.normal)
        self.oSlider.value = 0.0
        self.oOkyouView.addSubview(self.mTitleView)
    }
    
    //  MARK: - Method
    //    --------------------------------------------------------------------------------
    //    makeOkyouImage(sender:)
    //    --------------------------------------------------------------------------------
    func makeOkyouImage(page: Int) {
        let width = (self.mOkyouImg?.size.width)! - CGFloat(self.mPageWidth * Float(page))
        self.oOkyouView.image = self.mOkyouImg!.cropping(to: CGRect(x: 0, y: 0, width: width, height: (self.mOkyouImg?.size.height)!))
    }
    
    //    --------------------------------------------------------------------------------
    //    updateTime()
    //    --------------------------------------------------------------------------------
    @objc func updateTime() {
        var curTime: Float!
        if self.mOkyouSound.isPlaying {
            curTime = Float(self.mOkyouSound.currentTime)
            self.oSlider.value = curTime
        } else {
            curTime = self.oSlider.value
        }
        //聖人一流章
        if self.kyouten! == .shounin {
            if Int(curTime) > 46 {
                makeOkyouImage(page: 3)
            } else if Int(curTime) > 29 {
                makeOkyouImage(page: 2)
            } else if Int(curTime) > 13 {
                makeOkyouImage(page: 1)
            } else {
                makeOkyouImage(page: 0)
            }
        }
        //讃仏偈
        if self.kyouten! == .sanbutuge {
            makeOkyouImage(page: 0)
            if Int(curTime) > 115 {
                makeOkyouImage(page: 13)
            } else if Int(curTime) > 106 {
                makeOkyouImage(page: 12)
            } else if Int(curTime) > 99 {
                makeOkyouImage(page: 11)
            } else if Int(curTime) > 91 {
                makeOkyouImage(page: 10)
            } else if Int(curTime) > 83 {
                makeOkyouImage(page: 9)
            } else if Int(curTime) > 75 {
                makeOkyouImage(page: 8)
            } else if Int(curTime) > 67 {
                makeOkyouImage(page: 7)
            } else if Int(curTime) > 59 {
                makeOkyouImage(page: 6)
            } else if Int(curTime) > 5 {
                makeOkyouImage(page: 5)
            } else if Int(curTime) > 43 {
                makeOkyouImage(page: 4)
            } else if Int(curTime) > 35 {
                makeOkyouImage(page: 3)
            } else if Int(curTime) > 27 {
                makeOkyouImage(page: 2)
            } else if Int(curTime) > 18 {
                makeOkyouImage(page: 1)
            } else {
                makeOkyouImage(page: 0)
            }
        }
        //正信偈
        if self.kyouten! == .shoushinge {
            if Int(curTime) > 698 {
                makeOkyouImage(page: 32)
            } else if Int(curTime) > 659 {
                makeOkyouImage(page: 31)
            } else if Int(curTime) > 635 {
                makeOkyouImage(page: 30)
            } else if Int(curTime) > 600 {
                makeOkyouImage(page: 29)
            } else if Int(curTime) > 575 {
                makeOkyouImage(page: 28)
            } else if Int(curTime) > 546 {
                makeOkyouImage(page: 27)
            } else if Int(curTime) > 523 {
                makeOkyouImage(page: 26)
            } else if Int(curTime) > 492 {
                makeOkyouImage(page: 25)
            } else if Int(curTime) > 468 {
                makeOkyouImage(page: 24)
            } else if Int(curTime) > 442 {
                makeOkyouImage(page: 23)
            } else if Int(curTime) > 418 {
                makeOkyouImage(page: 22)
            } else if Int(curTime) > 388 {
                makeOkyouImage(page: 21)
            } else if Int(curTime) > 358 {
                makeOkyouImage(page: 20)
            } else if Int(curTime) > 326 {
                makeOkyouImage(page: 19)
            } else if Int(curTime) > 309 {
                makeOkyouImage(page: 18)
            } else if Int(curTime) > 291 {
                makeOkyouImage(page: 17)
            } else if Int(curTime) > 275 {
                makeOkyouImage(page: 16)
            } else if Int(curTime) > 246 {
                makeOkyouImage(page: 15)
            } else if Int(curTime) > 231 {
                makeOkyouImage(page: 14)
            } else if Int(curTime) > 216 {
                makeOkyouImage(page: 13)
            } else if Int(curTime) > 200 {
                makeOkyouImage(page: 12)
            } else if Int(curTime) > 185 {
                makeOkyouImage(page: 11)
            } else if Int(curTime) > 169 {
                makeOkyouImage(page: 10)
            } else if Int(curTime) > 152 {
                makeOkyouImage(page: 9)
            } else if Int(curTime) > 136 {
                makeOkyouImage(page: 8)
            } else if Int(curTime) > 119 {
                makeOkyouImage(page: 7)
            } else if Int(curTime) > 103 {
                makeOkyouImage(page: 6)
            } else if Int(curTime) > 87 {
                makeOkyouImage(page: 5)
            } else if Int(curTime) > 71 {
                makeOkyouImage(page: 4)
            } else if Int(curTime) > 56 {
                makeOkyouImage(page: 3)
            } else if Int(curTime) > 39 {
                makeOkyouImage(page: 2)
            } else if Int(curTime) > 23 {
                makeOkyouImage(page: 1)
            } else {
                makeOkyouImage(page: 0)
            }
        }
        //仏説阿弥陀経
        if self.kyouten! == .amidakyou {
            if Int(curTime) > 455 {
                makeOkyouImage(page: 74)
            } else if Int(curTime) > 447{
                makeOkyouImage(page: 73)
            } else if Int(curTime) > 440 {
                makeOkyouImage(page: 72)
            } else if Int(curTime) > 436 {
                makeOkyouImage(page: 71)
            } else if Int(curTime) > 430 {
                makeOkyouImage(page: 70)
            } else if Int(curTime) > 426 {
                makeOkyouImage(page: 69)
            } else if Int(curTime) > 420 {
                makeOkyouImage(page: 68)
            } else if Int(curTime) > 416 {
                makeOkyouImage(page: 67)
            } else if Int(curTime) > 410 {
                makeOkyouImage(page: 66)
            } else if Int(curTime) > 404 {
                makeOkyouImage(page: 65)
            } else if Int(curTime) > 400 {
                makeOkyouImage(page: 64)
            } else if Int(curTime) > 393 {
                makeOkyouImage(page: 63)
            } else if Int(curTime) > 388 {
                makeOkyouImage(page: 62)
            } else if Int(curTime) > 383 {
                makeOkyouImage(page: 61)
            } else if Int(curTime) > 377 {
                makeOkyouImage(page: 60)
            } else if Int(curTime) > 372 {
                makeOkyouImage(page: 59)
            } else if Int(curTime) > 366 {
                makeOkyouImage(page: 58)
            } else if Int(curTime) > 362 {
                makeOkyouImage(page: 57)
            } else if Int(curTime) > 356 {
                makeOkyouImage(page: 56)
            } else if Int(curTime) > 350 {
                makeOkyouImage(page: 55)
            } else if Int(curTime) > 346 {
                makeOkyouImage(page: 54)
            } else if Int(curTime) > 341 {
                makeOkyouImage(page: 53)
            } else if Int(curTime) > 335 {
                makeOkyouImage(page: 52)
            } else if Int(curTime) > 331 {
                makeOkyouImage(page: 51)
            } else if Int(curTime) > 326 {
                makeOkyouImage(page: 50)
            } else if Int(curTime) > 320 {
                makeOkyouImage(page: 49)
            } else if Int(curTime) > 316 {
                makeOkyouImage(page: 48)
            } else if Int(curTime) > 311 {
                makeOkyouImage(page: 47)
            } else if Int(curTime) > 304 {
                makeOkyouImage(page: 46)
            } else if Int(curTime) > 299 {
                makeOkyouImage(page: 45)
            } else if Int(curTime) > 294 {
                makeOkyouImage(page: 44)
            } else if Int(curTime) > 288 {
                makeOkyouImage(page: 43)
            } else if Int(curTime) > 283 {
                makeOkyouImage(page: 42)
            } else if Int(curTime) > 278 {
                makeOkyouImage(page: 41)
            } else if Int(curTime) > 272 {
                makeOkyouImage(page: 40)
            } else if Int(curTime) > 267 {
                makeOkyouImage(page: 39)
            } else if Int(curTime) > 262 {
                makeOkyouImage(page: 38)
            } else if Int(curTime) > 257 {
                makeOkyouImage(page: 37)
            } else if Int(curTime) > 252 {
                makeOkyouImage(page: 36)
            } else if Int(curTime) > 247 {
                makeOkyouImage(page: 35)
            } else if Int(curTime) > 242 {
                makeOkyouImage(page: 34)
            } else if Int(curTime) > 236 {
                makeOkyouImage(page: 33)
            } else if Int(curTime) > 230 {
                makeOkyouImage(page: 32)
            } else if Int(curTime) > 224 {
                makeOkyouImage(page: 31)
            } else if Int(curTime) > 218 {
                makeOkyouImage(page: 30)
            } else if Int(curTime) > 211 {
                makeOkyouImage(page: 29)
            } else if Int(curTime) > 204 {
                makeOkyouImage(page: 28)
            } else if Int(curTime) > 194 {
                makeOkyouImage(page: 27)
            } else if Int(curTime) > 169 {
                makeOkyouImage(page: 26)
            } else if Int(curTime) > 163 {
                makeOkyouImage(page: 25)
            } else if Int(curTime) > 157 {
                makeOkyouImage(page: 24)
            } else if Int(curTime) > 152 {
                makeOkyouImage(page: 23)
            } else if Int(curTime) > 146 {
                makeOkyouImage(page: 22)
            } else if Int(curTime) > 140 {
                makeOkyouImage(page: 21)
            } else if Int(curTime) > 135 {
                makeOkyouImage(page: 20)
            } else if Int(curTime) > 129 {
                makeOkyouImage(page: 19)
            } else if Int(curTime) > 124 {
                makeOkyouImage(page: 18)
            } else if Int(curTime) > 118 {
                makeOkyouImage(page: 17)
            } else if Int(curTime) > 113 {
                makeOkyouImage(page: 16)
            } else if Int(curTime) > 107 {
                makeOkyouImage(page: 15)
            } else if Int(curTime) > 102 {
                makeOkyouImage(page: 14)
            } else if Int(curTime) > 97 {
                makeOkyouImage(page: 13)
            } else if Int(curTime) > 91 {
                makeOkyouImage(page: 12)
            } else if Int(curTime) > 85 {
                makeOkyouImage(page: 11)
            } else if Int(curTime) > 80 {
                makeOkyouImage(page: 10)
            } else if Int(curTime) > 74 {
                makeOkyouImage(page: 9)
            } else if Int(curTime) > 68 {
                makeOkyouImage(page: 8)
            } else if Int(curTime) > 63 {
                makeOkyouImage(page: 7)
            } else if Int(curTime) > 57 {
                makeOkyouImage(page: 6)
            } else if Int(curTime) > 49 {
                makeOkyouImage(page: 5)
            } else if Int(curTime) > 42 {
                makeOkyouImage(page: 4)
            } else if Int(curTime) > 36 {
                makeOkyouImage(page: 3)
            } else if Int(curTime) > 29 {
                makeOkyouImage(page: 2)
            } else if Int(curTime) > 19 {
                makeOkyouImage(page: 1)
            } else {
                makeOkyouImage(page: 0)
            }
        }
        //重誓偈
        if self.kyouten! == .jyuseige {
            if Int(curTime) > 87 {
                makeOkyouImage(page: 7)
            } else if Int(curTime) > 76 {
                makeOkyouImage(page: 6)
            } else if Int(curTime) > 64 {
                makeOkyouImage(page: 5)
            } else if Int(curTime) > 54 {
                makeOkyouImage(page: 4)
            } else if Int(curTime) > 41 {
                makeOkyouImage(page: 3)
            } else if Int(curTime) > 29 {
                makeOkyouImage(page: 2)
            } else if Int(curTime) > 18 {
                makeOkyouImage(page: 1)
            } else {
                makeOkyouImage(page: 0)
            }
        }
    }
}
