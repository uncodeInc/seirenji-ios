//
//  SHouyouViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/07.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SHouyouViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var oScrollView: UIScrollView!
    @IBOutlet weak var oFirstDateTextField: UITextField!
    @IBOutlet weak var oSecondDateTextField: UITextField!
    @IBOutlet weak var oThirdDateTextField: UITextField!
    @IBOutlet weak var oAddressTextField: UITextField!
    @IBOutlet weak var oPhoneNumTextField: UITextField!
    @IBOutlet weak var oChiefNameTextField: UITextField!
    @IBOutlet weak var oJoinNumTextField: UITextField!
    
    
    var mDatePicker: UIDatePicker!
    var toolBar: UIToolbar!
    var mActiveTextField: UITextField!
    var mDate: NSDate? = nil
    var lastOffsetY: CGFloat = 0.0
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        //    NavigationBarButton作成
        self.navigationItem.title = "設　定"
        self.navigationItem.hidesBackButton = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.oScrollView.delegate = self
        self.oFirstDateTextField.delegate = self
        self.oSecondDateTextField.delegate = self
        self.oThirdDateTextField.delegate = self
        self.oAddressTextField.delegate = self
        self.oPhoneNumTextField.delegate = self
        self.oChiefNameTextField.delegate = self
        self.oJoinNumTextField.delegate = self
        
        //        self.oNameTextField.text = PrefManager.deceasedName
        //        self.oKaimyoTextField.text = PrefManager.deceasedKaimyo
        //        print(PrefManager.deceasedDate as Any)
        //        if PrefManager.deceasedDate != nil {
        //            self.oDateTextField.text = self.dateToString(date: PrefManager.deceasedDate! as NSDate)
        //        }
        
        // UIDatePickerの設定
        self.mDatePicker = UIDatePicker()
        self.mDatePicker.addTarget(self, action: #selector(changedDateEvent(_:)), for: .valueChanged)
        self.mDatePicker.datePickerMode = UIDatePickerMode.date
        self.mDatePicker.setDate(Date(), animated: false)
        self.mDatePicker.minimumDate = Date()
        self.mDatePicker.locale = Locale(identifier: "ja_JP")
        self.oFirstDateTextField.inputView = self.mDatePicker
        self.oSecondDateTextField.inputView = self.mDatePicker
        self.oThirdDateTextField.inputView = self.mDatePicker
        
        //datepicker上のtoolbarの生年月日入力ボタン
        self.toolBar = UIToolbar()
        self.toolBar.sizeToFit()
        
        let toolBarBtn = UIBarButtonItem(title: "完了", style: .plain, target: self, action: #selector(doneBtn))
        toolBar.items = [toolBarBtn]
        oFirstDateTextField.inputAccessoryView = toolBar
        oSecondDateTextField.inputAccessoryView = toolBar
        oThirdDateTextField.inputAccessoryView = toolBar
        
        
        
        //        let toolBarBtn = UIBarButtonItem(image: UIImage(named: "btn_close_tool.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(finishedToInputDate))
        //        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        //        self.mToolBar.items = [flexibleItem, toolBarBtn]
        //        self.oFirstDateTextField.inputAccessoryView = self.mToolBar
        //        self.oSecondDateTextField.inputAccessoryView = self.mToolBar
        //        self.oThirdDateTextField.inputAccessoryView = self.mToolBar
    }
    
    //toolbarの完了ボタン
    @objc func doneBtn(){
        oFirstDateTextField.resignFirstResponder()
        oSecondDateTextField.resignFirstResponder()
        oThirdDateTextField.resignFirstResponder()
    }
    //    --------------------------------------------------------------------------------
    //    viewWillAppear(_ animated:)
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //キーボードの登場と退場の通知
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.handleKeyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.handleKeyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //  ピッカービューの値が変わったら、テキストフィールドに表示
    //    --------------------------------------------------------------------------------
    //    changedDateEvent(_ sender:)
    //    --------------------------------------------------------------------------------
    @objc func changedDateEvent(_ sender: UIDatePicker){
        self.mDate = self.mDatePicker.date as NSDate
        self.mActiveTextField.text = self.dateToString(date: self.mDatePicker.date as NSDate)
    }
    
    //    --------------------------------------------------------------------------------
    //    dateToString(date:)
    //    --------------------------------------------------------------------------------
    func dateToString(date: NSDate) ->String {
        let date_formatter: DateFormatter = DateFormatter()
        date_formatter.locale     = NSLocale(localeIdentifier: "ja") as Locale!
        date_formatter.dateFormat = "yyyy年M月d日"
        return date_formatter.string(from: date as Date)
    }
    
    //    --------------------------------------------------------------------------------
    //    finishedToInputDate()
    //    --------------------------------------------------------------------------------
    @objc func finishedToInputDate() {
        self.oFirstDateTextField.resignFirstResponder()
        self.oSecondDateTextField.resignFirstResponder()
        self.oThirdDateTextField.resignFirstResponder()
    }
    
    //    --------------------------------------------------------------------------------
    //    handleKeyboardWillShowNotification(_ notification:)
    //    --------------------------------------------------------------------------------
    //キーボード登場通知を受けた際の処理
    @objc func handleKeyboardWillShowNotification(_ notification: Notification) {
        lastOffsetY = oScrollView.contentOffset.y
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let boundSize: CGSize = UIScreen.main.bounds.size
        let txtLimit = self.mActiveTextField.frame.origin.y + self.mActiveTextField.frame.height + 8.0
        let kbdLimit = boundSize.height - keyboardScreenEndFrame.size.height - 40.0
        
        print("テキストフィールドの下辺：(\(txtLimit))")
        print("キーボードの上辺：(\(kbdLimit))")
        
        if txtLimit >= kbdLimit {
            self.oScrollView.contentOffset.y = txtLimit - kbdLimit + 40.0
        }
    }
    
    //キーボード退場通知を受けた際の処理
    @objc func handleKeyboardWillHideNotification(_ notification: Notification) {
        self.oScrollView.contentOffset.y = lastOffsetY
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    submitAction(sender:)
    //    --------------------------------------------------------------------------------
    //    @IBAction func submitAction(sender: UIButton) {
    //        PrefManager.deceasedName = self.oNameTextField.text!
    //        PrefManager.deceasedKaimyo = self.oKaimyoTextField.text!
    //        if self.mDate != nil {
    //            PrefManager.deceasedDate = self.mDate! as Date
    //        }
    //        _ = self.navigationController?.popViewController(animated: false)
    //    }
    
    //  Return入力でキーボードを下げる
    //    --------------------------------------------------------------------------------
    //    textFieldShouldReturn(_ textField:)
    //    --------------------------------------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.oAddressTextField.resignFirstResponder()
        self.oPhoneNumTextField.resignFirstResponder()
        self.oChiefNameTextField.resignFirstResponder()
        self.oJoinNumTextField.resignFirstResponder()
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    textFieldShouldBeginEditing(_ textField:)
    //    --------------------------------------------------------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.mActiveTextField = textField
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    touchesBegan(_ touches:with event:)
    //    --------------------------------------------------------------------------------
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.oFirstDateTextField.resignFirstResponder()
        self.oSecondDateTextField.resignFirstResponder()
        self.oThirdDateTextField.resignFirstResponder()
        self.oAddressTextField.resignFirstResponder()
        self.oPhoneNumTextField.resignFirstResponder()
        self.oChiefNameTextField.resignFirstResponder()
        self.oJoinNumTextField.resignFirstResponder()
    }
}

extension UIScrollView {
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
    }
}
