//
//  SImageChoiceViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/07.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import Foundation

class SImageChoiceViewController: UIViewController {
    enum actionTag: Int {
        case action0 = 0
        case action1 = 1
        case action2 = 2
        case action3 = 3
        case action4 = 4
        case action5 = 5
        case action6 = 6
        case action7 = 7
    }
    
    @IBOutlet weak var oSegmentControll: UISegmentedControl!
    //  仏画
    @IBOutlet weak var oButugaLeftOutside: UIButton!
    @IBOutlet weak var oButugaLeft: UIButton!
    @IBOutlet weak var oButugaRight: UIButton!
    @IBOutlet weak var oButugaRightOutside: UIButton!
    //  遺影
    @IBOutlet weak var oIeiLeftOutside: UIButton!
    @IBOutlet weak var oIeiLeft: UIButton!
    @IBOutlet weak var oIeiRight: UIButton!
    @IBOutlet weak var oIeiRightOutside: UIButton!
    //  仏画選択後画像
    @IBOutlet weak var oButugaLeftOutsideChecked: UIImageView!
    @IBOutlet weak var oButugaLeftChecked: UIImageView!
    @IBOutlet weak var oButugaRightChecked: UIImageView!
    @IBOutlet weak var oButugaRightOutsideChecked: UIImageView!
    //  遺影選択後画像
    @IBOutlet weak var oIeiLeftOutsideChecked: UIImageView!
    @IBOutlet weak var oIeiLeftChecked: UIImageView!
    @IBOutlet weak var oIeiRightChecked: UIImageView!
    @IBOutlet weak var oIeiRightOutsideChecked: UIImageView!
    //  buttonを８つ設置・レシオは仏画と四つ切り
    
    //  buttonにサーバから取ってきた画像を表示
    
    //  洗濯後の画像を表示しておく
    
    //  選択後の画像は、画像が選択されるまで非表示
    
    //  選ばれた画像はprefへ保存・選択後の画像を表示する
//    @IBAction func buttonAction(_ sender: Any) {
//        if let button = sender as? UIButton {
//            if let tag = actionTag(rawValue: button.tag)
//        }   switch tag {
//        case action0:
//            PrefManager.butugaLeftOutside = oButugaLeftOutside.imageView!
//        default:
//            <#code#>
//        }
//    }
    
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if PrefManager.isDisplayPhoto {
            self.oSegmentControll.selectedSegmentIndex = 0
        } else {
            self.oSegmentControll.selectedSegmentIndex = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        oButugaLeftOutsideChecked.isHidden = true
        oButugaLeftChecked.isHidden = true
        oButugaRightChecked.isHidden = true
        oButugaRightOutsideChecked.isHidden = true
        
        oIeiLeftOutsideChecked.isHidden = true
        oIeiLeftChecked.isHidden = true
        oIeiRightChecked.isHidden = true
        oIeiRightOutsideChecked.isHidden = true
    }
    
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    topDisplayAction(sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func topDisplayAction(sender: UISegmentedControl) {
        //セグメント番号で条件分岐させる
        switch sender.selectedSegmentIndex {
        case 0:
            PrefManager.isDisplayPhoto = true
        case 1:
            PrefManager.isDisplayPhoto = false
        default:
            break
        }
    }
}
