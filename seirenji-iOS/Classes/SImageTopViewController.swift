import UIKit

class SImageTopViewController: UIViewController, UIApplicationDelegate {
    
    var isGetPoint = false
    var flowerImg: UIImageView!
    var aboutPointContentView: UIView!
    var isShownAboutPoint = false
    var currentMonth: String!
    var mCount = 0
    var mTimer: Timer!
    
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var oTopImageBtn: UIButton!
    
    @IBOutlet weak var oLeftPicImageView: UIImageView!
    @IBOutlet weak var oRightPicImageView: UIImageView!
    
    @IBOutlet weak var oOrinButton: UIButton!
    @IBOutlet weak var oOkouButton: UIButton!
    @IBOutlet weak var oKyouhonButton: UIButton!
    @IBOutlet weak var oPointButton: UIButton!
    @IBOutlet weak var oOtasukeButton: UIButton!
    @IBOutlet weak var oMemoryButton: UIButton!
    
    @IBOutlet weak var oFlowerImageView: UIImageView!
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBarButton作成
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "title_cocoro.png"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: 125, height: 10)
        titleImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = titleImageView
        //  遷移先の”戻る”を消す
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //  このページの戻るボタンを消す
        self.navigationItem.hidesBackButton = true
        
        //  今の月を取得
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MM", options: 0, locale: Locale(identifier: "en_US"))
        currentMonth = dateFormatter.string(from: Date())
        print(currentMonth!)
        
        // トップビュースライドショー
        self.slideshow()
    }
    
    //    --------------------------------------------------------------------------------
    //    viewWillAppear()
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        if PrefManager.isDisplayPhoto {
//            moveToPhoto()
            let storyboard: UIStoryboard = self.storyboard!
            let nextView = storyboard.instantiateViewController(withIdentifier: "PhotoView")
            let navi = UINavigationController(rootViewController: nextView)
            self.present(navi, animated: false, completion: nil)
        }
    }
    
    //    --------------------------------------------------------------------------------
    //    viewDidAppear()
    //    --------------------------------------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
//        if isGetPoint == false {
//            //ポイント追加
//            PrefManager.visitPoint += 100
//            //ポイント表示
//            print(PrefManager.visitPoint)
//            showAlert()
//            isGetPoint = true
//        }
//        print(PrefManager.isGetPoint)
    }
    
    //  MARK: - Method
    //    --------------------------------------------------------------------------------
    //    slideshow()
    //    --------------------------------------------------------------------------------
    func slideshow() {
        mTimer = Timer.scheduledTimer(
            timeInterval: 4,
            target: self,
            selector: #selector(self.slideChange),
            userInfo: nil,
            repeats: true)
    }

    @objc func slideChange() {
        if self.mCount == 0 {
            if PrefManager.deceasedIei1 != "" {
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                    self.oTopImageBtn.alpha = 0.5
                }, completion: { (finished) -> Void in
                    self.oTopImageBtn.setBackgroundImage(UIImage(data: PrefManager.deceasedIeiPhoto1)!, for: UIControlState.normal)
                    self.oTopImageBtn.alpha = 1
                    self.mCount = 1
                })
            } else {
                self.mCount = 1
            }
        }
        if self.mCount == 1 {
            if PrefManager.deceasedIei2 != "" {
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                    self.oTopImageBtn.alpha = 0.5
                }, completion: { (finished) -> Void in
                    self.oTopImageBtn.setBackgroundImage(UIImage(data: PrefManager.deceasedIeiPhoto2)!, for: UIControlState.normal)
                    self.oTopImageBtn.alpha = 1
                    self.mCount = 2
                })
            } else {
                self.mCount = 2
            }
        }
        if self.mCount == 2 {
            if PrefManager.deceasedIei3 != "" {
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                    self.oTopImageBtn.alpha = 0.5
                }, completion: { (finished) -> Void in
                    self.oTopImageBtn.setBackgroundImage(UIImage(data: PrefManager.deceasedIeiPhoto3)!, for: UIControlState.normal)
                    self.oTopImageBtn.alpha = 1
                    self.mCount = 3
                })
            } else {
                self.mCount = 3
            }
        }
        if self.mCount == 3 {
            if PrefManager.deceasedIei4 != "" {
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                    self.oTopImageBtn.alpha = 0.5
                }, completion: { (finished) -> Void in
                    self.oTopImageBtn.setBackgroundImage(UIImage(data: PrefManager.deceasedIeiPhoto4)!, for: UIControlState.normal)
                    self.oTopImageBtn.alpha = 1
                    self.mCount = 4
                })
            } else {
                self.mCount = 4
            }
        }
        if self.mCount == 3 {
            if PrefManager.deceasedIei4 != "" {
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                    self.oTopImageBtn.alpha = 0.5
                }, completion: { (finished) -> Void in
                    self.oTopImageBtn.setBackgroundImage(UIImage(data: PrefManager.deceasedIeiPhoto4)!, for: UIControlState.normal)
                    self.oTopImageBtn.alpha = 1
                    self.mCount = 4
                })
            } else {
                self.mCount = 4
            }
        }
        if self.mCount == 4 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                self.oTopImageBtn.alpha = 0.5
            }, completion: { (finished) -> Void in
                self.oTopImageBtn.setBackgroundImage(UIImage(named: "butuga_01.png"), for: UIControlState.normal)
                self.oTopImageBtn.alpha = 1
                self.mCount = 5
            })
        }
        if self.mCount == 5 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                self.oTopImageBtn.alpha = 0.5
            }, completion: { (finished) -> Void in
                self.oTopImageBtn.setBackgroundImage(UIImage(named: "butuga_02.png"), for: UIControlState.normal)
                self.oTopImageBtn.alpha = 1
                self.mCount = 6
            })
        }
        if self.mCount == 6 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                self.oTopImageBtn.alpha = 0.5
            }, completion: { (finished) -> Void in
                self.oTopImageBtn.setBackgroundImage(UIImage(named: "butuga_03.png"), for: UIControlState.normal)
                self.oTopImageBtn.alpha = 1
                self.mCount = 7
            })
        }
        if self.mCount == 7 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                self.oTopImageBtn.alpha = 0.5
            }, completion: { (finished) -> Void in
                self.oTopImageBtn.setBackgroundImage(UIImage(named: "butuga_04.png"), for: UIControlState.normal)
                self.oTopImageBtn.alpha = 1
                self.mCount = 8
            })
        }
        if self.mCount == 8 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
                self.oTopImageBtn.alpha = 0.5
            }, completion: { (finished) -> Void in
                self.oTopImageBtn.setBackgroundImage(UIImage(named: "butuga_05.png"), for: UIControlState.normal)
                self.oTopImageBtn.alpha = 1
                self.mCount = 0
            })
        }
    }

    
    //    --------------------------------------------------------------------------------
    //    moveToPhoto()
    //    --------------------------------------------------------------------------------
    func moveToPhoto() {
        self.performSegue(withIdentifier: "moveToPhoto", sender: self)
    }
    
    //ポイント追加のアラート
    //    --------------------------------------------------------------------------------
    //    showAlert()
    //    --------------------------------------------------------------------------------
    func showAlert() {
        let alert:UIAlertController = UIAlertController(title: "100ポイント取得しました", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    AboutPointAction(sender:)
    //    --------------------------------------------------------------------------------
    
    //写真をタップすると
    
    //元の画面が消える
    
    //元の画面の高さはoffsetに保存
    
    //instantiateviewで紐ずけていたviewを表示
    
    //遷移先の写真をタップすると遷移元に戻る
//    @IBAction func AboutPointAction(sender: UIButton) {
//        let offset = self.view.frame.height
//        if self.isShownAboutPoint {
//            self.isShownAboutPoint = false
////            self.oScroolView.isScrollEnabled = true
//            UIView.animate(withDuration: 0.3,
//                           delay: 0.0,
//                           options: UIViewAnimationOptions.curveLinear,
//                           animations: { () -> Void in
//                            self.oLeftPicImageView.alpha = 0.3
//                            self.oRightPicImageView.alpha = 0.3
//                            self.oOrinButton.alpha = 0.3
//                            self.oOkouButton.alpha = 0.3
//                            self.oPointButton.alpha = 0.3
//                            self.oOtasukeButton.alpha = 0.3
//                            self.oMemoryButton.alpha = 0.3
//                            self.aboutPointContentView.alpha = 0
//                            self.rotateImage(image: self.oArrowImageView, angle: 0.0)
//                            self.view.layoutIfNeeded()
//            }, completion: { (finished) -> Void in
//                self.aboutPointContentView.removeFromSuperview()
//            })
//            UIView.animate(withDuration: 0.4,
//                           delay: 0.1,
//                           options: UIViewAnimationOptions.curveEaseInOut,
//                           animations: { () -> Void in
//                            self.oTitleImageView.alpha = 1
//                            self.oGetPointButton.alpha = 1
//                            self.oPointLabel.alpha = 1
//                            self.oRegisterButton.alpha = 1
//                            self.oScroolView.contentOffset = CGPoint(x: 0, y: 0)
//            }, completion: { (finished) -> Void in
//            })
//        } else {
//            self.isShownAboutPoint = true
//            self.oScroolView.isScrollEnabled = false
//            self.aboutPointContentView = storyboard!.instantiateViewController(withIdentifier: "aboutPointView").view
//            self.aboutPointContentView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 55)
//            self.aboutPointContentView.alpha = 0
//            let sv = self.aboutPointContentView as! UIScrollView
//            sv.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.width / 1300.0 * 3734.0)
//            self.view.addSubview(self.aboutPointContentView)
//            self.oHeaderView.sendSubview(toBack: self.oTitleImageView)
//            self.oHeaderView.sendSubview(toBack: self.oGetPointButton)
//            self.oHeaderView.sendSubview(toBack: self.aboutPointContentView)
//
//            UIView.animate(withDuration: 0.5,
//                           delay: 0.0,
//                           options: UIViewAnimationOptions.curveEaseInOut,
//                           animations: { () -> Void in
//                            self.aboutPointContentView.alpha = 1
//                            self.oScroolView.contentOffset = CGPoint(x: 0, y: offset)
//                            self.oTitleImageView.alpha = 0
//                            self.oGetPointButton.alpha = 0
//                            self.oPointLabel.alpha = 0
//                            self.oRegisterButton.alpha = 0
//                            self.rotateImage(image: self.oArrowImageView, angle: 90.0)
//                            self.view.layoutIfNeeded()
//            }, completion: { (finished) -> Void in
//
//            })
//        }
//    }

    
    
}
