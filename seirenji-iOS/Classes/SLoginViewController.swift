//
//  SLoginViewController.swift
//  seirenji-iOS
//
//  Created by noru on 2018/05/22.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import KRProgressHUD

class SLoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var oIdTextField: UITextField!
    @IBOutlet weak var oPasswordTextField: UITextField!
    
//    private let dURL = "http://192.168.1.17:3000/cocoro/api"
    private let dURL = "https://cocoro-dev.herokuapp.com/cocoro/api"

    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.oIdTextField.delegate = self
        self.oPasswordTextField.delegate = self
    }

    //    --------------------------------------------------------------------------------
    //    viewWillAppear(_ animated: Bool)
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        if !PrefManager.userID.isEmpty {
            self.getKojinInfo()
            let storyboard: UIStoryboard = self.storyboard!
            var nextView = storyboard.instantiateViewController(withIdentifier: "ImageView")
            if PrefManager.isDisplayPhoto {
                nextView = storyboard.instantiateViewController(withIdentifier: "PhotoView")
            }
            let navi = UINavigationController(rootViewController: nextView)
            // アニメーションの設定
            navi.modalTransitionStyle = .crossDissolve
            self.present(navi, animated: true, completion: nil)
        }
    }
    
    //    --------------------------------------------------------------------------------
    //    getKojinInfo()
    //    --------------------------------------------------------------------------------
    func getKojinInfo() {
        var postString  = "userID=" + PrefManager.userID
        postString += "&password=" + PrefManager.password
        var request = URLRequest(url: URL(string: dURL)!)
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (jsonData, response, error) in
            do {
                KRProgressHUD.dismiss()
                guard error == nil else {
                    let alert:UIAlertController = UIAlertController(title: "サーバに接続できません", message: "ネット環境を確認してください", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                let jsonDict = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! NSDictionary
                
                DispatchQueue.main.async {
                    if (jsonDict["kojin"] as? NSString)?.integerValue == 0 {
                        let alert:UIAlertController = UIAlertController(title: "ユーザーIDもしくはパスワードが違います。", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let kojin = jsonDict["kojin"] as! NSDictionary
                        PrefManager.userID = kojin["user_id"] as! String
                        PrefManager.password = kojin["password"] as! String
                        let wareki = kojin["date_of_death_wareki"] as! String
                        let month = kojin["date_of_death_month"] as! String
                        let day = kojin["date_of_death_day"] as! String
                        PrefManager.deceasedDateWareki = wareki + month + day
                        PrefManager.deceasedName = kojin["houmyou"] as! String
                        var photoDict = kojin["photo1"] as! NSDictionary
                        var imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei1 = url
                        } else {
                            PrefManager.deceasedIei1 = ""
                        }
                        photoDict = kojin["photo2"] as! NSDictionary
                        imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei2 = url
                        } else {
                            PrefManager.deceasedIei2 = ""
                        }
                        photoDict = kojin["photo3"] as! NSDictionary
                        imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei3 = url
                        } else {
                            PrefManager.deceasedIei3 = ""
                        }
                        photoDict = kojin["photo4"] as! NSDictionary
                        imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei4 = url
                        } else {
                            PrefManager.deceasedIei4 = ""
                        }
                        PrefManager.deceasedZokumyou = kojin["zokumyou"] as! String
                        PrefManager.deceasedOtera = kojin["shzokuji"] as! String
                        PrefManager.deceasedOteraTel = kojin["shozokuji_tel"] as! String
                        
                        //遺影
                        if PrefManager.deceasedIei1 != "" {
                            let url = URL(string : PrefManager.deceasedIei1)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto1 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto1 =  Data(base64Encoded: "")!
                        }
                        if PrefManager.deceasedIei2 != "" {
                            let url = URL(string : PrefManager.deceasedIei2)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto2 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto2 =  Data(base64Encoded: "")!
                        }
                        if PrefManager.deceasedIei3 != "" {
                            let url = URL(string : PrefManager.deceasedIei3)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto3 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto3 =  Data(base64Encoded: "")!
                        }
                        if PrefManager.deceasedIei4 != "" {
                            let url = URL(string : PrefManager.deceasedIei4)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto4 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto4 =  Data(base64Encoded: "")!
                        }
                        let storyboard: UIStoryboard = self.storyboard!
                        let nextView = storyboard.instantiateViewController(withIdentifier: "ImageView")
                        let navi = UINavigationController(rootViewController: nextView)
                        self.present(navi, animated: true, completion: nil)
                    }
                }
            }
            catch {
                let alert:UIAlertController = UIAlertController(title: "サーバに接続できません", message: "ネット環境を確認してください", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        })
        task.resume()
        KRProgressHUD.show(withMessage: "通信中...")
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    loginAction(sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func loginAction(sender: UIButton) {
        var postString  = "userID=" + self.oIdTextField.text!
        postString += "&password=" + self.oPasswordTextField.text!
        var request = URLRequest(url: URL(string: dURL)!)
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (jsonData, response, error) in
            do {
                KRProgressHUD.dismiss()
                guard error == nil else {
                    let alert:UIAlertController = UIAlertController(title: "サーバに接続できません", message: "ネット環境を確認してください", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                let jsonDict = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as! NSDictionary
                
                DispatchQueue.main.async {
                    if (jsonDict["kojin"] as? NSString)?.integerValue == 0 {
                        let alert:UIAlertController = UIAlertController(title: "ユーザーIDもしくはパスワードが違います。", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let kojin = jsonDict["kojin"] as! NSDictionary
                        PrefManager.userID = kojin["user_id"] as! String
                        PrefManager.password = kojin["password"] as! String
                        let wareki = kojin["date_of_death_wareki"] as! String
                        let month = kojin["date_of_death_month"] as! String
                        let day = kojin["date_of_death_day"] as! String
                        PrefManager.deceasedDateWareki = wareki + month + day
                        PrefManager.deceasedName = kojin["houmyou"] as! String
                        var photoDict = kojin["photo1"] as! NSDictionary
                        var imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei1 = url
                        } else {
                            PrefManager.deceasedIei1 = ""
                        }
                        photoDict = kojin["photo2"] as! NSDictionary
                        imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei2 = url
                        } else {
                            PrefManager.deceasedIei2 = ""
                        }
                        photoDict = kojin["photo3"] as! NSDictionary
                        imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei3 = url
                        } else {
                            PrefManager.deceasedIei3 = ""
                        }
                        photoDict = kojin["photo4"] as! NSDictionary
                        imgtrimDict = photoDict["imgtrim"] as! NSDictionary
                        if let url = imgtrimDict["url"] as? String {
                            PrefManager.deceasedIei4 = url
                        } else {
                            PrefManager.deceasedIei4 = ""
                        }
                        PrefManager.deceasedZokumyou = kojin["zokumyou"] as! String
                        PrefManager.deceasedOtera = kojin["shzokuji"] as! String
                        PrefManager.deceasedOteraTel = kojin["shozokuji_tel"] as! String
                        let storyboard: UIStoryboard = self.storyboard!
                        let nextView = storyboard.instantiateViewController(withIdentifier: "ImageView")
                        let navi = UINavigationController(rootViewController: nextView)
                        self.present(navi, animated: true, completion: nil)
                        
                        //遺影
                        if PrefManager.deceasedIei1 != "" {
                            let url = URL(string : PrefManager.deceasedIei1)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto1 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto1 =  Data(base64Encoded: "")!
                        }
                        if PrefManager.deceasedIei2 != "" {
                            let url = URL(string : PrefManager.deceasedIei2)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto2 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto2 =  Data(base64Encoded: "")!
                        }
                        if PrefManager.deceasedIei3 != "" {
                            let url = URL(string : PrefManager.deceasedIei3)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto3 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto3 =  Data(base64Encoded: "")!
                        }
                        if PrefManager.deceasedIei4 != "" {
                            let url = URL(string : PrefManager.deceasedIei4)!
                            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                do {
                                    KRProgressHUD.dismiss()
                                    guard error == nil else {
                                        print("can't retrieve")
                                        return
                                    }
                                    PrefManager.deceasedIeiPhoto4 = data!
                                }
                            })
                            task.resume()
                            KRProgressHUD.show(withMessage: "通信中...")
                        } else {
                            PrefManager.deceasedIeiPhoto4 =  Data(base64Encoded: "")!
                        }
                    }
                }
            }
            catch {
                let alert:UIAlertController = UIAlertController(title: "サーバに接続できません", message: "ネット環境を確認してください", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        })
        task.resume()
        KRProgressHUD.show(withMessage: "通信中...")
    }
    
    //  MARK: - Delegate
    //    --------------------------------------------------------------------------------
    //    textFieldShouldReturn(_ textField:)
    //    --------------------------------------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.oIdTextField.resignFirstResponder()
        self.oPasswordTextField.resignFirstResponder()
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    touchesBegan(_ touches:with event:)
    //    --------------------------------------------------------------------------------
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.oIdTextField.resignFirstResponder()
        self.oPasswordTextField.resignFirstResponder()
    }

}
