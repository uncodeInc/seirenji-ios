//
//  SMemoryEditViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/14.
//  Copyright © 2018年 aki. All rights reserved.
//

import Foundation
import UIKit
import Photos
import RealmSwift

class SMemoryEditViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate {
    
    
    //  1.画像、タイトル、内容が入力
    @IBOutlet weak var oSetImage: UIImageView!
    @IBOutlet weak var oSetTitle: UITextField!
    @IBOutlet weak var oSetContents: UITextView!
    @IBOutlet weak var oScrollView: UIScrollView!
    
    var lastOffsetY: CGFloat = 0.0
    var pickImage: String? = ""
    var mActiveTextField: UITextField!
    var mIsChangeImage = false
    var mLastOffsetY: CGFloat = 0.0
    var mIsEditingTextField = false

    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oSetContents.delegate = self
        oSetTitle.delegate = self
        oSetImage.contentMode = .scaleAspectFit
        oSetImage.image = UIImage(named: "default_image.png")
    }
    
    //    --------------------------------------------------------------------------------
    //    viewWillAppear()
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        //キーボードの登場と退場の通知
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.handleKeyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.handleKeyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //  MARK: - Method
    //    --------------------------------------------------------------------------------
    //    showPhotoLibrary()
    //    --------------------------------------------------------------------------------
    func showPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let pickerView = UIImagePickerController()
            pickerView.sourceType = .photoLibrary
            pickerView.delegate = self
            self.present(pickerView, animated: true)
        }
    }
    
    //    --------------------------------------------------------------------------------
    //    handleKeyboardWillShowNotification(_ notification:)
    //    --------------------------------------------------------------------------------
    //キーボード登場通知を受けた際の処理
    @objc func handleKeyboardWillShowNotification(_ notification: Notification) {
        self.lastOffsetY = oScrollView.contentOffset.y
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let boundSize: CGSize = UIScreen.main.bounds.size
        var txtLimit = self.oSetContents.frame.origin.y + self.oSetContents.frame.height + 20.0
        if self.mIsEditingTextField {
            txtLimit = self.oSetTitle.frame.origin.y + self.oSetTitle.frame.height + 20.0
        }
        
        let kbdLimit = boundSize.height - keyboardScreenEndFrame.size.height - 40.0
        
        print("テキストフィールドの下辺：(\(txtLimit))")
        print("キーボードの上辺：(\(kbdLimit))")
        
        if txtLimit >= kbdLimit {
            self.oScrollView.contentOffset.y = txtLimit - kbdLimit + 40.0
        }
    }
    //キーボード退場通知を受けた際の処理
    @objc func handleKeyboardWillHideNotification(_ notification: Notification) {
        self.oScrollView.contentOffset.y = lastOffsetY
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    pickImageAction(_ sender: )
    //    --------------------------------------------------------------------------------
    @IBAction func pickImageAction(_ sender: UIButton) {
        showPhotoLibrary()
        let alert: UIAlertController = UIAlertController(title: "表示する画像を選択してください。", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            return
        })
    }
    
    //    --------------------------------------------------------------------------------
    //    registerAction(_ sender: )
    //    --------------------------------------------------------------------------------
    @IBAction func registerAction(_ sender: UIButton) {
        let db = try! Realm()
        let memory = Memory()
        memory.title = self.oSetTitle.text
        memory.content = self.oSetContents.text
        if self.mIsChangeImage {
            memory.picture = UIImagePNGRepresentation(self.oSetImage.image!.resized(toWidth: 500)!)
        } else {
            memory.picture = Data(base64Encoded: "")!
        }
        try! db.write {
            db.add(memory, update: false)
            let alert:UIAlertController = UIAlertController(title: "故人の思い出を登録しました。", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "一覧へ戻る", style: UIAlertActionStyle.default, handler: {(action) in self.navigationController?.popViewController(animated: true)}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //  MARK: - Delegate
    //    --------------------------------------------------------------------------------
    //    imagePickerController(_ picker: , didFinishPickingMediaWithInfo info: )
    //    --------------------------------------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.mIsChangeImage = true
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.oSetImage.image = image
        self.dismiss(animated: true)
    }
    
    //    --------------------------------------------------------------------------------
    //    textFieldShouldReturn(_ textField: ) -> Bool
    //    --------------------------------------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        oSetTitle.resignFirstResponder()
        oSetContents.resignFirstResponder()
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    textFieldShouldBeginEditing(_ textField:)
    //    --------------------------------------------------------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.mIsEditingTextField = true
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.mIsEditingTextField = false
    }
    
    //    --------------------------------------------------------------------------------
    //    touchesBegan(_ touches:with event:)
    //    --------------------------------------------------------------------------------
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        oSetTitle.resignFirstResponder()
        oSetContents.resignFirstResponder()
    }
    
    //  テキストビューが入力状態になった時: キーボードが登場したら
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        lastOffsetY = oScrollView.contentOffset.y
//        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = ([UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let boundSize: CGSize = UIScreen.main.bounds.size
        let txtLimit = self.mActiveTextField.frame.origin.y + self.mActiveTextField.frame.height + 8.0
        let kbdLimit = boundSize.height - keyboardScreenEndFrame.size.height - 40.0
        
        print("テキストフィールドの下辺：(\(txtLimit))")
        print("キーボードの上辺：(\(kbdLimit))")
        
        if txtLimit >= kbdLimit {
            self.oScrollView.contentOffset.y = txtLimit - kbdLimit + 40.0
        }

    }

    
    //  ビューがキーボードと重なっていたら
    
    //  スクロールする
}

extension UIImage {
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
