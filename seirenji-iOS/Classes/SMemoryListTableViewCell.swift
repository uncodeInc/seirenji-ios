//
//  SMemoryListTableViewCell.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/14.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SMemoryListTableViewCell: UITableViewCell {

    @IBOutlet weak var oMemoryImageView: UIImageView!
    @IBOutlet weak var oTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        oTitleLabel.font = UIFont.systemFont(ofSize: 30)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
