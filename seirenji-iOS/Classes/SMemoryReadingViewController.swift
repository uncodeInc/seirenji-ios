//
//  SMemoryReadingViewController.swift
//  seirenji-iOS
//
//  Created by noru on 2018/05/23.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import RealmSwift

class SMemoryReadingViewController: UIViewController {

    @IBOutlet weak var oImage: UIImageView!
    @IBOutlet weak var oTitleLabel: UILabel!
    @IBOutlet weak var oContentsTextFiled: UITextView!
    
    var memory: Memory!
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.oImage.image = UIImage(data: self.memory.picture!)
        self.oTitleLabel.text = self.memory.title
        self.oContentsTextFiled.text = self.memory.content
    }
}
