//
//  SMemoryViewController.swift
//  seirenji-iOS
    //  
//  Created by aki on 2018/05/14.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import RealmSwift

class SMemoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var oTableView: UITableView!
    @IBOutlet weak var oNoMemoryLable: UILabel!
    
    var memories: [Memory]! = []
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //    --------------------------------------------------------------------------------
    //    viewWillAppear()
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        self.oTableView.reloadData()
    }
    
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //  MARK: - Delegate
    //    --------------------------------------------------------------------------------
    //    tableView(_ tableView: , numberOfRowsInSection section: ) -> Int
    //    --------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let db = try! Realm()
        let memories = db.objects(Memory.self)
        if memories.count != 0 {
            self.oNoMemoryLable.isHidden = true
        }
        return memories.count
    }
    
    //    --------------------------------------------------------------------------------
    //    tableView(_ tableView: , cellForRowAt indexPath: ) -> UITableViewCell
    //    --------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as! SMemoryListTableViewCell
        let db = try! Realm()
        let memory = db.objects(Memory.self)[indexPath.row]
        if memory.picture != nil {
            cell.oMemoryImageView?.image = UIImage(data: memory.picture!)
        }
        cell.oTitleLabel?.text = memory.title
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    //    --------------------------------------------------------------------------------
    //    tableView(tableView:didSelectRowAtIndexPath:)
    //    --------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MemoryView") as! SMemoryReadingViewController
        let db = try! Realm()
        vc.memory = db.objects(Memory.self)[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //    --------------------------------------------------------------------------------
    //    tableView(_ tableView:canEditRowAt indexPath:)
    //    --------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    tableView(_ tableView:editActionsForRowAt indexPath:)
    //    --------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton: UITableViewRowAction = UITableViewRowAction(style: .normal, title: "削除") { (action, index) -> Void in
            let alert:UIAlertController = UIAlertController(title: "削除しますか？", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "はい", style: UIAlertActionStyle.default, handler: {(action) in
                let db = try! Realm()
                let memory = db.objects(Memory.self)[indexPath.row]
                try! db.write() {
                    db.delete(memory)
                }
                self.oTableView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        deleteButton.backgroundColor = UIColor.red
        
        return [deleteButton]
    }
    
    
}
