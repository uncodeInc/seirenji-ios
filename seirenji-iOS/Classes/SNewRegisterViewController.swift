//
//  SNewRegisterViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/25.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SNewRegisterViewController: UIViewController {
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBarButton作成
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "chara_otasuke.png"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: 125, height: 10)
        titleImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = titleImageView
        //  遷移先の”戻る”を消す
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // MARK: - Navigation

}
