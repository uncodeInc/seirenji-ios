//
//  SOkyouhonViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/25.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import KRProgressHUD

class SOkyouhonViewController: UIViewController {
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  遷移先の”戻る”を消す
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //  ナビゲーションバーに文字を表示
        self.navigationItem.title = "読　経"
        //  文字のサイズ、色、フォントを指定
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont(name: "HiraKakuProN-W3", size: 22)!]
    }
    
    //    --------------------------------------------------------------------------------
    //    viewWillAppear(_ animated:)
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        KRProgressHUD.dismiss()
    }

    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //  MARK: - Segue
    //    --------------------------------------------------------------------------------
    //    prepare(for segue:sender:) {
    //    --------------------------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc: SAmidakyouViewController = (segue.destination as? SAmidakyouViewController)!
        if segue.identifier == "shoushinge" {
            vc.kyouten = .shoushinge
        } else if segue.identifier == "amidakyou" {
            vc.kyouten = .amidakyou
        } else if segue.identifier == "sanbutuge" {
            vc.kyouten = .sanbutuge
        } else if segue.identifier == "jyuseige" {
            vc.kyouten = .jyuseige
        } else if segue.identifier == "shounin" {
            vc.kyouten = .shounin
        }
    }
}
