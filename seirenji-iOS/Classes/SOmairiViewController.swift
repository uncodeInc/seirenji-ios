//
//  SOmairiViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/17.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SOmairiViewController: UIViewController {

    @IBOutlet weak var oOmairiView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url: NSURL = NSURL(string: ConstStruct.youtube_url)!
        let request: NSURLRequest = NSURLRequest(url: url as URL)
        oOmairiView.loadRequest(request as URLRequest)
        oOmairiView.backgroundColor = .red
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

