//
//  SOrinViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/25.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift
import youtube_ios_player_helper
import KRProgressHUD

class SOrinViewController: UIViewController, YTPlayerViewDelegate {
    
    @IBOutlet weak var oSenkouView: UIView!
    @IBOutlet weak var oSenkouBtn: UIButton!
    
    var mSenkouPlayer: AVPlayer!
    
    let dateFormatter = DateFormatter()
    
    private let dMovieRatio = 374.0 / 620.0
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBarButton作成
        self.navigationItem.title = "お鈴・お香"
        self.navigationItem.hidesBackButton = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "ydMMM", options: 0, locale: Locale(identifier: "ja_JP"))
        print(dateFormatter.string(from: Date()))
    }
    
    //    --------------------------------------------------------------------------------
    //    viewDidLayoutSubviews()
    //    --------------------------------------------------------------------------------
    override func viewDidLayoutSubviews() {
        KRProgressHUD.dismiss()
        self.prepareSenkou()
    }
    
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //  MARK: - Method
    //    --------------------------------------------------------------------------------
    //    prepareSenkou()
    //    --------------------------------------------------------------------------------
    func prepareSenkou() {
        //線香動画準備
        let bundlePath = Bundle.main.path(forResource: "senkou", ofType: "mp4")
        let url = URL(fileURLWithPath: bundlePath!)
        self.mSenkouPlayer = AVPlayer(url: url)
        NotificationCenter.default.addObserver(self,selector:#selector(SOrinViewController.playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.mSenkouPlayer)
        let playerLayer = AVPlayerLayer(player: self.mSenkouPlayer)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        let viewWidth = Double(self.oSenkouView.frame.width)
        let viewHeight = Double(self.oSenkouView.frame.height)
        let movieHeight = viewHeight * 0.9
        playerLayer.frame.size = CGSize(width: movieHeight * dMovieRatio, height: movieHeight)
        let movieOriginX = (viewWidth - movieHeight * dMovieRatio) / 2
        let movieOriginY = (viewHeight - movieHeight) / 2
        playerLayer.frame.origin = CGPoint(x: movieOriginX, y: movieOriginY)
        
        self.oSenkouView.layer.addSublayer(playerLayer)
    }
    
    //    --------------------------------------------------------------------------------
    //    playerDidFinishPlaying()
    //    --------------------------------------------------------------------------------
    @objc func playerDidFinishPlaying() {
        self.oSenkouBtn.isEnabled = true
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    rinAction(_ sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func rinAction(_ sender: Any) {
        mPlayerRin.currentTime = 0
        mPlayerRin.play()
    }
    
    //    --------------------------------------------------------------------------------
    //    senkouBtnAction(sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func senkouBtnAction(sender: UIButton) {
        self.mSenkouPlayer.play()
        self.oSenkouBtn.isEnabled = true
    }
    
}
