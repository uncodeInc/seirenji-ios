//
//  SPhotoTopViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/24.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SPhotoTopViewController: UIViewController {
    
    var isGetPoint = false
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBarButton作成
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "chara_cocoro.png"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: 125, height: 10)
        titleImageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = titleImageView
        
        //  遷移先の”戻る”を消す
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //  このページの戻るボタンを消す
        self.navigationItem.hidesBackButton = true
        
        // トップビュースライドショー
        self.slideshow()
    }
    
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //    --------------------------------------------------------------------------------
    //    viewWillAppear()
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        if !PrefManager.isDisplayPhoto {
//            moveToIllustration();
            let storyboard: UIStoryboard = self.storyboard!
            let nextView = storyboard.instantiateViewController(withIdentifier: "ImageView")
            let navi = UINavigationController(rootViewController: nextView)
            self.present(navi, animated: false, completion: nil)
        }
    }
    
    //  MARK: - Method
    //    --------------------------------------------------------------------------------
    //    slideshow()
    //    --------------------------------------------------------------------------------
    func slideshow() {
        
    }
    
    //    --------------------------------------------------------------------------------
    //    moveToIllustration()
    //    --------------------------------------------------------------------------------
    func moveToIllustration() {
        self.performSegue(withIdentifier: "moveToIllustration", sender: self)
    }
    
    //ポイント追加のアラート
    //    --------------------------------------------------------------------------------
    //    showAlert()
    //    --------------------------------------------------------------------------------
    func showAlert() {
        let alert:UIAlertController = UIAlertController(title: "100ポイント取得しました", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}
