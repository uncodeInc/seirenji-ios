//
//  SPointViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/25.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SPointViewController: UIViewController {
    @IBOutlet weak var oPointLable: UILabel!
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  ナビゲーションバーに文字を表示
        self.navigationItem.title = "ポイント"
        //  文字のサイズ、色、フォントを指定
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont(name: "HiraKakuProN-W3", size: 22)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // ポイント表示
        self.oPointLable.text = String(PrefManager.visitPoint) + "pt"

    }
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
