import UIKit

class SRegisterPageViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var oScrollView: UIScrollView!
    
    @IBOutlet weak var oDateTextField: UITextField!
    @IBOutlet weak var oNameTextField: UITextField!
    @IBOutlet weak var oZokumyouTextField: UITextField!
    @IBOutlet weak var oOteraTextField: UITextField!
    
    @IBOutlet weak var oIei1ImageView: UIImageView!
    @IBOutlet weak var oIei2ImageView: UIImageView!
    @IBOutlet weak var oIei3ImageView: UIImageView!
    @IBOutlet weak var oIei4ImageView: UIImageView!
    
    
    var mDatePicker: UIDatePicker!
    var mToolBar: UIToolbar!
    var mActiveTextField: UITextField!
    var mDate: NSDate? = nil
    var toolBar:UIToolbar!
    var lastOffsetY: CGFloat = 0.0
    
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBarButton作成
        self.navigationItem.title = "設　定"
        self.navigationItem.hidesBackButton = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.oDateTextField.delegate = self
        self.oNameTextField.delegate = self
        self.oZokumyouTextField.delegate = self
        self.oOteraTextField.delegate = self
        
        self.oNameTextField.text = PrefManager.deceasedName
        self.oZokumyouTextField.text = PrefManager.deceasedZokumyou
        self.oOteraTextField.text = PrefManager.deceasedOtera
        self.oDateTextField.text = PrefManager.deceasedDateWareki as! String
        
        // UIDatePickerの設定
        self.mDatePicker = UIDatePicker()
        self.mDatePicker.addTarget(self, action: #selector(changedDateEvent(_:)), for: .valueChanged)
        self.mDatePicker.datePickerMode = UIDatePickerMode.date
        self.mDatePicker.setDate(Date(), animated: false)
        self.mDatePicker.maximumDate = Date()
        self.mDatePicker.locale = Locale(identifier: "ja_JP")
        self.oDateTextField.inputView = self.mDatePicker
        
        //datepicker上のtoolbarの生年月日入力ボタン
        toolBar = UIToolbar()
        toolBar.sizeToFit()

        let toolBarBtn = UIBarButtonItem(title: "完了", style: .plain, target: self, action: #selector(doneBtn))
        toolBar.items = [toolBarBtn]
        oDateTextField.inputAccessoryView = toolBar

//        let toolBarBtn = UIBarButtonItem(image: UIImage(named: "btn_close_tool.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(finishedToInputDate))
//        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        self.mToolBar.items = [flexibleItem, toolBarBtn]
//        self.oDateTextField.inputAccessoryView = self.mToolBar
        
        //遺影
        if PrefManager.deceasedIei1 != "" {
            self.oIei1ImageView.image = UIImage(data: PrefManager.deceasedIeiPhoto1)
            self.oIei1ImageView.setNeedsLayout()
        } else {
            self.oIei1ImageView.isHidden = true;
        }
        if PrefManager.deceasedIei2 != "" {
            self.oIei2ImageView.image = UIImage(data: PrefManager.deceasedIeiPhoto2)
            self.oIei2ImageView.setNeedsLayout()
        } else {
            self.oIei2ImageView.isHidden = true;
        }
        if PrefManager.deceasedIei3 != "" {
            self.oIei3ImageView.image = UIImage(data: PrefManager.deceasedIeiPhoto3)
            self.oIei3ImageView.setNeedsLayout()
        } else {
            self.oIei3ImageView.isHidden = true;
        }
        if PrefManager.deceasedIei4 != "" {
            self.oIei4ImageView.image = UIImage(data: PrefManager.deceasedIeiPhoto4)
            self.oIei4ImageView.setNeedsLayout()
        } else {
            self.oIei4ImageView.isHidden = true;
        }
    }
    
    //toolbarの完了ボタン
    @objc func doneBtn(){
        oDateTextField.resignFirstResponder()
    }

    //    --------------------------------------------------------------------------------
    //    viewWillAppear(_ animated:)
    //    --------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //キーボードの登場と退場の通知
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.handleKeyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.handleKeyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //  //  ピッカービューの値が変わったら、テキストフィールドに表示
    //    --------------------------------------------------------------------------------
    //    changedDateEvent(_ sender:)
    //    --------------------------------------------------------------------------------
    @objc func changedDateEvent(_ sender: UIDatePicker){
        self.mDate = self.mDatePicker.date as NSDate
        self.oDateTextField.text = self.dateToString(date: self.mDatePicker.date as NSDate)
    }
    
    //    --------------------------------------------------------------------------------
    //    dateToString(date:)
    //    --------------------------------------------------------------------------------
    func dateToString(date: NSDate) ->String {
        let date_formatter: DateFormatter = DateFormatter()
        date_formatter.locale     = NSLocale(localeIdentifier: "ja") as Locale!
        date_formatter.dateFormat = "yyyy年M月d日"
        return date_formatter.string(from: date as Date)
    }
    
    //    --------------------------------------------------------------------------------
    //    finishedToInputDate()
    //    --------------------------------------------------------------------------------
    @objc func finishedToInputDate() {
        self.oDateTextField.resignFirstResponder()
    }
    
    //    --------------------------------------------------------------------------------
    //    handleKeyboardWillShowNotification(_ notification:)
    //    --------------------------------------------------------------------------------
    //    キーボード登場通知を受けた際の処理
    @objc func handleKeyboardWillShowNotification(_ notification: Notification) {
        lastOffsetY = oScrollView.contentOffset.y
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let boundSize: CGSize = UIScreen.main.bounds.size
        let txtLimit = self.mActiveTextField.frame.origin.y + self.mActiveTextField.frame.height + 8.0
        let kbdLimit = boundSize.height - keyboardScreenEndFrame.size.height - 40.0
        
        print("テキストフィールドの下辺：(\(txtLimit))")
        print("キーボードの上辺：(\(kbdLimit))")
        
        if txtLimit >= kbdLimit {
            self.oScrollView.contentOffset.y = txtLimit - kbdLimit + 40.0
        }
    }
    //    キーボード退場通知を受けた際の処理
    @objc func handleKeyboardWillHideNotification(_ notification: Notification) {
        self.oScrollView.contentOffset.y = lastOffsetY
    }
    
    //  MARK: - Action
    //    --------------------------------------------------------------------------------
    //    submitAction(sender:)
    //    --------------------------------------------------------------------------------
    @IBAction func submitAction(sender: UIButton) {
        PrefManager.deceasedName = self.oNameTextField.text!
        PrefManager.deceasedZokumyou = self.oZokumyouTextField.text!
        PrefManager.deceasedOtera = self.oOteraTextField.text!
        if self.mDate != nil {
            PrefManager.deceasedDate = self.mDate! as Date
        }
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    //  MARK: - delegate
    //    --------------------------------------------------------------------------------
    //    textFieldShouldReturn(_ textField:)
    //    --------------------------------------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.oNameTextField.resignFirstResponder()
        self.oZokumyouTextField.resignFirstResponder()
        self.oOteraTextField.resignFirstResponder()
        self.oDateTextField.resignFirstResponder()
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    textFieldShouldBeginEditing(_ textField:)
    //    --------------------------------------------------------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.mActiveTextField = textField
        return true
    }
    
    //    --------------------------------------------------------------------------------
    //    touchesBegan(_ touches:with event:)
    //    --------------------------------------------------------------------------------
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.oNameTextField.resignFirstResponder()
        self.oZokumyouTextField.resignFirstResponder()
        self.oOteraTextField.resignFirstResponder()
        self.oDateTextField.resignFirstResponder()
    }
}
