//
//  SReservationInfoViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/08.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SReservationInfoViewController: UIViewController {
    //    --------------------------------------------------------------------------------
    //    viewDidLoad()
    //    --------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  遷移先の”戻る”を消す
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }
    //    --------------------------------------------------------------------------------
    //    didReceiveMemoryWarning()
    //    --------------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
