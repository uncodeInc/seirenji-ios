//
//  SSplashViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/25.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class SSplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //  MARK: - Method
    //    --------------------------------------------------------------------------------
    //    moveToMenuView()
    //    --------------------------------------------------------------------------------
    func moveToMenuView() {
        self.performSegue(withIdentifier: "moveToMenuView", sender: self)
    }
}
