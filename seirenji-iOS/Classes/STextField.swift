//
//  STextField.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/23.
//  Copyright © 2018年 aki. All rights reserved.
//

import UIKit

class STextField: UITextField {
    
    override func draw(_ rect: CGRect) {
        self.layer.borderWidth = 1.5
        self.layer.borderColor = UIColor(red: 148/255, green: 104/255, blue: 169/255, alpha: 1.0).cgColor
    }
}
