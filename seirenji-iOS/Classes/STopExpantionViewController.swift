//
//  STopExpantionViewController.swift
//  seirenji-iOS
//
//  Created by aki on 2018/05/21.
//  Copyright © 2018年 aki. All rights reserved.
//


extension DateFormatter {
    
    enum Template: String {
        case month = "M"
    }
    
    func setTemplate(_ template: Template) {
        dateFormat = DateFormatter.dateFormat(fromTemplate: template.rawValue, options: 0, locale: .current)
    }
}

import UIKit

class STopExpantionViewController: UIViewController {
    
    //  月判定で、花の画像を変更する処理
    let currentMonth = DateFormatter()
    let date = Date()
    //  遺影をprefから取ってきて表示

    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentMonth.setTemplate(.month)
//        print(currentMonth.string(from: date))
//        print(currentMonth)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


