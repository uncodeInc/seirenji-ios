//
//  PrefManager.swift
//  seirenji-iOS
//
//  Created by aki on 2018/04/25.
//  Copyright © 2018年 aki. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


class PrefManager {
    
    static let ud = UserDefaults.standard
    
    private struct Default {
        static let isRetrieving = false
        static let isMember = false
        static let isDisplayPhoto = true
        static let userID: String = ""
        static let password: String = ""
        
        static let deceasedDate: Date? = nil
        static let deceasedDateWareki: String = ""
        static let deceasedName: String = ""
        static let deceasedIei1: String = ""
        static let deceasedIei2: String = ""
        static let deceasedIei3: String = ""
        static let deceasedIei4: String = ""
        static let deceasedZokumyou: String = ""
        static let deceasedOtera: String = ""
        static let deceasedOteraTel: String = ""
        static let visitPoint: Int = 0
        static let isGetPoint = false
        static let memoryImage: String = ""
        static let memoryTitle: String = ""
        static let memoryContents: String = ""
        static let butugaLeftOutside: String = ""
        static let butugaLeft: String = ""
        static let butugaRight: String = ""
        static let butugaRightOutside: String = ""
        static let ieiLeftOutside: String = ""
        static let ieiLeft: String = ""
        static let ieiRight: String = ""
        static let ieiRightOutside: String = ""
        
        static let deceasedIeiPhoto1: Data = Data(base64Encoded: "")!
        static let deceasedIeiPhoto2: Data = Data(base64Encoded: "")!
        static let deceasedIeiPhoto3: Data = Data(base64Encoded: "")!
        static let deceasedIeiPhoto4: Data = Data(base64Encoded: "")!
    }
    
    class var isRetrieving: Bool {
        get {
            ud.register(defaults: ["isRetrieving": Default.isRetrieving])
            return ud.object(forKey: "isRetrieving") as! Bool
        }
        set(newValue) {
            ud.set(newValue, forKey: "isRetrieving")
        }
    }
    
    class var isMember: Bool {
        get {
            ud.register(defaults: ["isMember": Default.isMember])
            return ud.object(forKey: "isMember") as! Bool
        }
        set(newValue) {
            ud.set(newValue, forKey: "isMember")
        }
    }
    
    class var isDisplayPhoto: Bool {
        get {
            ud.register(defaults: ["isDisplayPhoto": Default.isDisplayPhoto])
            return ud.object(forKey: "isDisplayPhoto") as! Bool
        }
        set(newValue) {
            ud.set(newValue, forKey: "isDisplayPhoto")
        }
    }
    
    class var userID: String {
        get {
            ud.register(defaults: ["userID": Default.userID])
            return ud.object(forKey: "userID") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "userID")
        }
    }
    
    class var password: String {
        get {
            ud.register(defaults: ["password": Default.password])
            return ud.object(forKey: "password") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "password")
        }
    }

    class var deceasedDate: Date {
        get {
            ud.register(defaults: ["deceasedDate": Default.deceasedDate!])
            return ud.object(forKey: "deceasedDate") as! Date
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedDate")
        }
    }
    
    class var deceasedDateWareki: String {
        get {
            ud.register(defaults: ["deceasedDateWareki": Default.deceasedDateWareki])
            return ud.object(forKey: "deceasedDateWareki") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedDateWareki")
        }
    }

    
    class var deceasedName: String {
        get {
            ud.register(defaults: ["deceasedName": Default.deceasedName])
            return ud.object(forKey: "deceasedName") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedName")
        }
    }
    
    class var deceasedIei1: String {
        get {
            ud.register(defaults: ["deceasedIei1": Default.deceasedIei1])
            return ud.object(forKey: "deceasedIei1") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIei1")
        }
    }
    
    class var deceasedIei2: String {
        get {
            ud.register(defaults: ["deceasedIei2": Default.deceasedIei2])
            return ud.object(forKey: "deceasedIei2") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIei2")
        }
    }
    
    class var deceasedIei3: String {
        get {
            ud.register(defaults: ["deceasedIei3": Default.deceasedIei3])
            return ud.object(forKey: "deceasedIei3") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIei3")
        }
    }
    
    class var deceasedIei4: String {
        get {
            ud.register(defaults: ["deceasedIei4": Default.deceasedIei4])
            return ud.object(forKey: "deceasedIei4") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIei4")
        }
    }
    
    class var deceasedZokumyou: String {
        get {
            ud.register(defaults: ["deceasedZokumyou": Default.deceasedZokumyou])
            return ud.object(forKey: "deceasedZokumyou") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedZokumyou")
        }
    }
    
    class var deceasedOtera: String {
        get {
            ud.register(defaults: ["deceasedOtera": Default.deceasedOtera])
            return ud.object(forKey: "deceasedOtera") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedOtera")
        }
    }
    
    class var deceasedOteraTel: String {
        get {
            ud.register(defaults: ["deceasedOteraTel": Default.deceasedOteraTel])
            return ud.object(forKey: "deceasedOteraTel") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedOteraTel")
        }
    }
    
    
    class var visitPoint: Int {
        get {
            ud.register(defaults: ["visitPoint": Default.visitPoint])
            return ud.object(forKey: "visitPoint") as! Int
        }
        set(newValue) {
            ud.set(newValue, forKey: "visitPoint")
            ud.synchronize()

        }
    }
    
    class var isGetPoint: Bool {
        get {
            ud.register(defaults: ["isGetPoint": Default.isGetPoint])
            return ud.object(forKey: "isGetPoint") as! Bool
        }
        set(newValue) {
            ud.set(newValue, forKey: "isGetPoint")
        }
    }
    
    class var memoryImage: String {
        get {
            ud.register(defaults: ["memoryImage": Default.memoryImage])
            return ud.object(forKey: "memoryImage") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "memoryImage")
        }
    }
    
    class var memoryTitle: String {
        get {
            ud.register(defaults: ["memoryTitle": Default.memoryTitle])
            return ud.object(forKey: "memoryTitle") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "memoryTitle")
        }
    }
    
    class var memoryContents: String {
        get {
            ud.register(defaults: ["memoryContents": Default.memoryContents])
            return ud.object(forKey: "memoryContents") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "memoryContents")
        }
    }
    
    class var butugaLeftOutside: String {
        get {
            ud.register(defaults: ["butugaLeftOutside": Default.butugaLeftOutside])
            return ud.object(forKey: "butugaLeftOutside") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "butugaLeftOutside")
        }
    }
    
    class var butugaLeft: String {
        get {
            ud.register(defaults: ["butugaLeft": Default.butugaLeft])
            return ud.object(forKey: "butugaLeft") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "butugaLeft")
        }
    }

    class var butugaRight: String {
        get {
            ud.register(defaults: ["butugaRight": Default.butugaRight])
            return ud.object(forKey: "butugaRight") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "butugaRight")
        }
    }

    class var butugaRightOutside: String {
        get {
            ud.register(defaults: ["butugaRightOutside": Default.butugaRightOutside])
            return ud.object(forKey: "butugaRightOutside") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "butugaRightOutside")
        }
    }
    
    class var ieiLeftOutside: String {
        get {
            ud.register(defaults: ["ieiLeftOutside": Default.ieiLeftOutside])
            return ud.object(forKey: "ieiLeftOutside") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "ieiLeftOutside")
        }
    }
    
    class var ieiLeft: String {
        get {
            ud.register(defaults: ["ieiLeft": Default.ieiLeft])
            return ud.object(forKey: "ieiLeft") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "ieiLeft")
        }
    }
    
    class var ieiRight: String {
        get {
            ud.register(defaults: ["ieiRight": Default.ieiRight])
            return ud.object(forKey: "ieiRight") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "ieiRight")
        }
    }
    
    class var ieiRightOutside: String {
        get {
            ud.register(defaults: ["ieiRightOutside": Default.ieiRightOutside])
            return ud.object(forKey: "ieiRightOutside") as! String
        }
        set(newValue) {
            ud.set(newValue, forKey: "ieiRightOutside")
        }
    }
    
    class var deceasedIeiPhoto1: Data {
        get {
            ud.register(defaults: ["deceasedIeiPhoto1": Default.deceasedIeiPhoto1])
            return ud.object(forKey: "deceasedIeiPhoto1") as! Data
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIeiPhoto1")
        }
    }
    
    class var deceasedIeiPhoto2: Data {
        get {
            ud.register(defaults: ["deceasedIeiPhoto2": Default.deceasedIeiPhoto2])
            return ud.object(forKey: "deceasedIeiPhoto2") as! Data
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIeiPhoto2")
        }
    }
    
    class var deceasedIeiPhoto3: Data {
        get {
            ud.register(defaults: ["deceasedIeiPhoto3": Default.deceasedIeiPhoto3])
            return ud.object(forKey: "deceasedIeiPhoto3") as! Data
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIeiPhoto3")
        }
    }
    
    class var deceasedIeiPhoto4: Data {
        get {
            ud.register(defaults: ["deceasedIeiPhoto4": Default.deceasedIeiPhoto4])
            return ud.object(forKey: "deceasedIeiPhoto4") as! Data
        }
        set(newValue) {
            ud.set(newValue, forKey: "deceasedIeiPhoto4")
        }
    }

}

